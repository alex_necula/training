<?php

session_start();

require_once 'config.php';

$dsn = "mysql:host=" . DB_HOST . ";dbname=" . DB_NAME . ";charset=utf8mb4";
$pdo = new PDO($dsn, DB_USERNAME, DB_PASS);

// Cart list has to be set
if (!isset($_SESSION['cartList'])) {
    $_SESSION['cartList'] = array();
}

/**
 * Get products that are or aren't in the cart
 *
 * @param bool $cart
 * @return array
 */
function getProductsInOrNotInCart($cart = false)
{
    global $pdo;
    $productIds = array_values($_SESSION['cartList']) ?: [];

    $placeHolders = array_fill(0, count($productIds), '?');
    $placeHolders = implode(',', $placeHolders);

    $sql = "SELECT * FROM products WHERE id " . ($cart ? '' : 'NOT') . " IN ($placeHolders)";
    $stmt = $pdo->prepare($sql);

    $stmt->execute($productIds);
    $products = $stmt->fetchAll();

    return $products;
}

/**
 * Get every product from the table
 *
 * @return array
 */
function getAllProducts()
{
    global $pdo;
    $sql = "SELECT * FROM products;";
    $stmt = $pdo->prepare($sql);

    $stmt->execute();
    $products = $stmt->fetchAll();

    return $products;
}

/**
 *  Convert special characters to HTML entities
 *
 * @param string $nonEscapedString
 * @return string
 */
function escape($nonEscapedString)
{
    return htmlspecialchars($nonEscapedString, ENT_QUOTES);
}

/**
 * Checks if the product id is present in database
 *
 * @param int $productId
 * @return bool
 */
function productExists($productId)
{
    global $pdo;
    $sql = "SELECT TRUE FROM products WHERE id = ?;";
    $stmt = $pdo->prepare($sql);

    $stmt->execute([$productId]);

    return ($stmt->rowCount() > 0);
}

/**
 * Return the translated word if there exists one, the word itself otherwise
 *
 * @param string $label
 * @param string $language
 * @return string
 */
function translate($label, $language = "en")
{
    // Uses translate.json to get the necessary word list
    $translations = file_get_contents('translate.json');
    $translations = json_decode($translations, true);

    return $translations[$language][$label] ?? $label;
}

/**
 * Deletes the image from server
 * Get the name from database record corresponding to given ID
 *
 * @param int $productId
 */
function deleteImage($productId)
{
    global $pdo;
    $siteRoot = realpath(dirname(__FILE__));

    $sqlOldImage = "SELECT image_name FROM products WHERE id = ?;";
    $stmt = $pdo->prepare($sqlOldImage);

    $stmt->execute([$productId]);
    $oldImage = $stmt->fetch();

    if (isset($oldImage[0]) && !empty($oldImage[0]) && file_exists($siteRoot . "/images/" . $oldImage[0])) {
        unlink($siteRoot . "/images/" . $oldImage[0]);
    }
}
