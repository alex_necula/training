<?php

require_once 'common.php';

if (!isset($_SESSION['username'])) {
    header('Location: /index.php');
    exit();
}

// If the 'delete' button is pressed
if (isset($_GET['id']) && productExists($_GET['id'])) {
    // Delete image
    deleteImage($_GET['id']);

    // Delete the record from database
    $sql = "DELETE FROM products WHERE id = ?;";
    $stmt = $pdo->prepare($sql);

    $stmt->execute([$_GET['id']]);
}

// If the logout button has been pressed
if (isset($_GET['logout'])) {
    session_unset();
    session_destroy();

    header('Location: /login.php');
    exit();
}

// Is an array with all info of every product
$products =  getAllProducts();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?= escape(translate("Products")) ?></title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <?php if (!empty($products)) : ?>
        <div id="productList">
            <?php foreach ($products as $product) : ?>
                <div class="product">
                    <img class="productImage" src="/images/<?= escape($product['image_name']) ?>" alt="<?= escape($product['title']) ?>">
                    <div class="productInfo">
                        <div class="productTitle"><?= escape($product['title']) ?></div>
                        <div class="productDescription"><?= escape($product['description']) ?></div>
                        <div class="productPrice"><?= escape($product['price']) ?></div>
                    </div>
                    <div class="edit_delete">
                        <a href="/product.php?id=<?= escape($product['id']) ?>"><?= escape(translate("Edit")) ?></a>
                        <a href="/products.php?id=<?= escape($product['id']) ?>"><?= escape(translate("Delete")) ?></a>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    <?php endif ?>

    <a href="/product.php"><?= escape(translate("Add")) ?></a>
    <a href="/products.php?logout"><?= escape(translate("Logout")) ?></a>
</body>
</html>
