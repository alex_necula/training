<?php

require_once 'common.php';

if (!isset($_SESSION['username'])) {
    header('Location: /index.php');
    exit();
}

$title = $description = $price = '';
$titleErrors = $priceErrors = $imageErrors = $globalErrors = array();

/**
 * If the 'edit' button from products is pressed, values from input will be auto-completed
 * Also checks if the id is present in database
 */
if (isset($_GET['id']) && productExists($_GET['id'])) {
    $sql = "SELECT title, description, price FROM products WHERE id = ?;";
    $stmt = $pdo->prepare($sql);

    $stmt->execute([$_GET['id']]);
    $product = $stmt->fetch(PDO::FETCH_ASSOC);

    $title = $product['title'];
    $description = $product['description'];
    $price = $product['price'];
}

// If the button 'save' has been pressed
if (isset($_POST['title'])) {
    // Fields' values should not change in case of errors
    $title = $_POST['title'];
    $description = $_POST['description'];
    $price = $_POST['price'];

    // URL has been modified
    if (isset($_GET['id']) && !productExists($_GET['id'])) {
        $globalErrors[] = translate('Product does not exist in database.');
    }

    // Title MUST NOT be empty
    if (strlen($_POST['title']) === 0) {
        $titleErrors[] = translate('Title should not be empty.');
    }

    // Price MUST NOT be empty and MUST be numeric
    if (strlen($_POST['price']) === 0) {
        $priceErrors[] = translate('Price field should not be empty.');
    } elseif (!is_numeric($_POST['price'])) {
        $priceErrors[] = translate('Price should be numeric.');
    }

    // If the 'Add' button is pressed and no image uploaded
    if ($_FILES['image']['error'] === 4 && !isset($_GET['id'])) {
        $imageErrors[] = translate('An image must be uploaded.');
    }

    /**
     * If the image is not valid, set an error
     * Extension has to be valid and size of image less or equal to 5 MB
     */
    if ($_FILES['image']['error'] !== 4) {
        if ($_FILES['image']['error'] !== 0) {
            $imageErrors[] = translate('There was an error with the image.') . $_FILES['image']['error'];
        }

        $validTypes = ["image/jpeg", "image/png"];
        if (!in_array($_FILES['image']['type'], $validTypes)) {
            $imageErrors[] = translate('Invalid image type.');
        }
        if ($_FILES['image']['size'] > 5368709120) {
            $imageErrors[] = translate('Image should have less than 5MB.');
        }
    }

    // No errors so far
    if (empty($globalErrors) && empty($imageErrors) && empty($titleErrors) && empty($priceErrors)) {
        $executeArray = array($_POST['title'], $_POST['description'], $_POST['price']);

        // An image has been uploaded
        if ($_FILES['image']['error'] !== 4) {
            $siteRoot = realpath(dirname(__FILE__));

            $fileExt = explode(".", $_FILES['image']['name']);
            $fileExt = strtolower(end($fileExt));
            $fileName = uniqid("image") . '.' . $fileExt;
            $fileDestination = $siteRoot . "/images/" . $fileName;

            // Move new image to the images directory
            if (!move_uploaded_file($_FILES['image']['tmp_name'], $fileDestination)) {
                $imageErrors[] = translate('Image could not be uploaded');
            } else {
                // Add image name to database
                $executeArray[] = $fileName;
                // Delete old image
                if (isset($_GET['id'])) {
                    deleteImage($_GET['id']);
                }
            }
        }
        if (empty($imageErrors)) {
            if (isset($_GET['id'])) {
                // 'Edit' button has been pressed
                $sql = "UPDATE products SET title = ?, description = ?, price = ?" . (($_FILES['image']['error'] !== 4) ? ", image_name = ?" : '') . " WHERE id = ?;";
                $executeArray[] = $_GET['id'];
            } else {
                // 'Add' button has been pressed
                $sql = "INSERT INTO products (title, description, price, image_name) VALUES (?, ?, ?, ?);";
            }

            $stmt = $pdo->prepare($sql);
            $stmt->execute($executeArray);
            // Redirect after success
            header('Location: /products.php');
            exit();
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?= escape(translate("Product Edit")) ?></title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <?php if (!empty($globalErrors)) : ?>
        <p class="error">
            <?php foreach ($globalErrors as $error) : ?>
                <?= escape($error) ?> <br>
            <?php endforeach ?>
        </p>
    <?php endif ?>

    <form method="POST" enctype="multipart/form-data">
        <input type="text" name="title" placeholder="<?= escape(translate("Title")) ?>" value="<?= escape($title) ?>">

        <?php if (!empty($titleErrors)) : ?>
            <p class="error">
                <?php foreach ($titleErrors as $error) : ?>
                    <?= escape($error) ?> <br>
                <?php endforeach ?>
            </p>
        <?php else : ?>
            <br><br>
        <?php endif ?>

        <input type="text" name="description" placeholder="<?= escape(translate("Description")) ?>" value="<?= escape($description) ?>"><br><br>
        <input type="text" name="price" placeholder="<?= escape(translate("Price")) ?>" value="<?= escape($price) ?>">

        <?php if (!empty($priceErrors)) : ?>
            <p class="error">
                <?php foreach ($priceErrors as $error) : ?>
                    <?= escape($error) ?> <br>
                <?php endforeach ?>
            </p>
        <?php else : ?>
            <br><br>
        <?php endif ?>

        <input type="file" name="image" value="<?= escape(translate("Browse")) ?>">

        <?php if (!empty($imageErrors)) : ?>
            <p class="error">
                <?php foreach ($imageErrors as $error) : ?>
                    <?= escape($error) ?> <br>
                <?php endforeach ?>
            </p>
        <?php else : ?>
            <br><br>
        <?php endif ?>

        <a href="/products.php"><?= escape(translate("Products")) ?></a>
        <button type="submit"><?= escape(translate("Save")) ?></button>
    </form>
</body>
</html>
