<?php

require_once 'common.php';

// Adds a product id to the list of products in cart
if (isset($_GET['id'])
    && !empty($_GET['id'])
    && productExists($_GET['id'])
    && !in_array($_GET['id'], $_SESSION['cartList'])) {

    $_SESSION['cartList'][] = $_GET['id'];
}

// Is an array with all info of products that are not in cart
$products = empty($_SESSION['cartList']) ? getAllProducts() : getProductsInOrNotInCart();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?= escape(translate('Shop')) ?></title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <?php if (isset($_SESSION['username'])) : ?>
        <a href="/products.php"><?= escape(translate("Go to products")) ?></a>
    <?php endif ?>

    <?php if (!empty($products)) : ?>
        <div id="productList">
            <?php foreach ($products as $product) : ?>
                <div class="product">
                    <img class="productImage" src="/images/<?= escape($product['image_name']) ?>" alt="<?= escape($product['title']) ?>">
                    <div class="productInfo">
                        <div class="productTitle"><?= escape($product['title']) ?></div>
                        <div class="productDescription"><?= escape($product['description']) ?></div>
                        <div class="productPrice"><?= escape($product['price']) ?></div>
                    </div>
                    <a href="index.php?id=<?= escape($product['id']) ?>"><?= escape(translate("Add")) ?></a>
                    <br>
                </div>
            <?php endforeach ?>
        </div>
    <?php endif ?>

    <a href="/cart.php"><?= escape(translate("Go to cart")) ?></a>
</body>
</html>
