<?php

require_once 'common.php';

if (!isset($_SESSION['username'])) {
    header('Location: /index.php');
    exit();
}

// Fetch all info of orders
$sql = 'SELECT o.id, o.name, o.contact, o.comments, o.created_at, SUM(p.price) AS total
        FROM product_order po
        JOIN products p ON po.product_id = p.id
        JOIN orders o ON po.order_id = o.id
        GROUP BY o.id, o.name, o.contact, o.comments, o.created_at
        ORDER BY o.created_at DESC;';
$stmt = $pdo->prepare($sql);

$stmt->execute();
$orders = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?= escape(translate("Orders")) ?></title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <?php foreach ($orders as $order) : ?>
        <div class="orderContainer">
            <div><?= translate('Order ID') . ': ' . escape($order['id']) ?></div><hr>
            <div><?= translate('Order Date') . ': ' . escape($order['created_at']) ?></div><hr>
            <div><?= translate('Client Name') . ': ' . escape($order['name']) ?></div><hr>
            <div><?= translate('Contact Details') . ': ' . escape($order['contact']) ?></div><hr>
            <div><?= translate('Order Comments') . ': ' . escape($order['comments']) ?></div><hr>
            <div><?= translate('Total') . ': ' . escape($order['total']) ?></div><hr>
            <a href="/order.php?id=<?= escape($order['id']) ?>"><?= escape(translate('Go to order')) ?></a>
        </div>
    <?php endforeach ?>
</body>
</html>
