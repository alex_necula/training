<?php

require_once 'common.php';

if (!isset($_SESSION['username'])) {
    header('Location: /index.php');
    exit();
}

// If user gets to this page without referencing an order id, redirect him
if (!isset($_GET['id']) || strlen($_GET['id']) === 0) {
    header('Location: /orders.php');
    exit();
}

$sql = 'SELECT o.id, o.name, o.contact, o.comments, o.created_at, p.title, p.description, p.price, p.image_name, (
            SELECT sum(p1.price)
            FROM product_order po1
            JOIN products p1 ON po1.product_id = p1.id
            WHERE po1.order_id = :id
        ) AS total
        FROM product_order po
        JOIN products p ON po.product_id = p.id
        JOIN orders o ON po.order_id = o.id
        WHERE po.order_id = :id;';

$stmt = $pdo->prepare($sql);

$stmt->execute(['id' => $_GET['id']]);
$orderInfo = $stmt->fetchAll(PDO::FETCH_ASSOC);

$errors = (count($orderInfo) === 0) ? array(translate('Order ID is invalid.')) : array();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?= escape(translate("Order details")) ?></title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <?php if (!empty($errors)) : ?>
        <p class="error">
            <?php foreach ($errors as $error) : ?>
                <?= escape($error) ?> <br>
            <?php endforeach ?>
        </p>
    <?php endif ?>

    <?php if (count($orderInfo) > 0) : ?>
        <div class="orderContainer">
            <div><?= translate('Order ID') . ': ' . escape($orderInfo[0]['id']) ?></div><hr>
            <div><?= translate('Order Date') . ': ' . escape($orderInfo[0]['created_at']) ?></div><hr>
            <div><?= translate('Client Name') . ': ' . escape($orderInfo[0]['name']) ?></div><hr>
            <div><?= translate('Contact Details') . ': ' . escape($orderInfo[0]['contact']) ?></div><hr>
            <div><?= translate('Order Comments') . ': ' . escape($orderInfo[0]['comments']) ?></div><hr>
            <div><?= translate('Total') . ': ' . escape($orderInfo[0]['total']) ?></div>
        </div>


        <?php foreach ($orderInfo as $product) : ?>
            <div class="product">
                <img class="productImage" src="/images/<?= escape($product['image_name']) ?>" alt="<?= escape($product['title']) ?>">
                <div class="productInfo">
                    <div class="productTitle"><?= escape($product['title']) ?></div>
                    <div class="productDescription"><?= escape($product['description']) ?></div>
                    <div class="productPrice"><?= escape($product['price']) ?></div>
                </div>
            </div>
        <?php endforeach ?>
    <?php endif ?>

    <a href="/orders.php"><?= escape(translate('Go to orders')) ?></a>
</body>
</html>
