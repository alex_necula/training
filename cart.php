<?php

require_once 'common.php';

// Initializing
$name = $contact = $comments = '';
$globalErrors = $nameErrors = $contactErrors = array();
$checkoutSuccess = false;

/**
 * If the 'Remove' button is pressed, the corresponding id will be unset form cart list
 * It also checks if the id actually IS in the cart list
 */
if (isset($_GET['id'])) {
    $removeKey = array_search($_GET['id'], $_SESSION['cartList']);

    if ($removeKey !== false) {
        unset($_SESSION['cartList'][$removeKey]);
    }
}

// Is an array with all info of products that are in cart
$products = empty($_SESSION['cartList']) ? array() : getProductsInOrNotInCart(true);

// Checks if the button 'checkout' has been pressed
if (isset($_POST['name'])) {
    // Fields' values should not change in case of error
    $name = $_POST['name'];
    $contact = $_POST['contact'];
    $comments = $_POST['comments'];

    // Cart should not be empty
    if (empty($_SESSION['cartList'])) {
        $globalErrors[] = translate('Error: Cart is empty.');
    }

    // Name field MUST NOT be empty and MUST have a minimum length
    if (strlen($_POST['name']) === 0) {
        $nameErrors[] = translate('Name field should not be empty.');
    } elseif (strlen($_POST['name']) < 3) {
        $nameErrors[] = translate('Name should be larger than two characters. Please write your real name.');
    }

    // Contact field MUST NOT be empty and MUST have a minimum length
    if (strlen($_POST['contact']) === 0) {
        $contactErrors[] = translate('Contact field should not be empty.');
    } elseif (strlen($_POST['contact']) < 6) {
        $contactErrors[] = translate('Contact should be larger than five characters. Please write real contact info like email or phone.');
    }

    // If fields are ok, send the mail
    if (empty($globalErrors) && empty($nameErrors) && empty($contactErrors)) {
        $subject = translate('New Command!');

        // Display the complete URL
        $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'];

        // Product list
        $productsList = '';
        foreach ($products as $product) {
            $productsList .= "<img width='50px' height='50px' alt='" . escape($product['title']) . "' src='" . $link . "/images/" . escape($product['image_name']) . "'><br>";
            $productsList .= "<p>" . translate('Title') . ": " . escape($product['title']) . "</p>";
            $productsList .= "<p>" . translate('Description') . ": " . escape($product['description']) . "</p>";
            $productsList .= "<p>" . translate('Price') . ": " . escape($product['price']) . "</p><hr>";
        }

        $message = "
        <html lang='en'>
        <head>
            <title>" . escape($subject) . "</title>
        </head>
        <body>
            <h1>" . translate('From') . ": " . escape($_POST['name']) . "</h1>
            <h2>" . translate('Contact') . ": " . escape($_POST['contact']) . "</h2>
            <h3>" . translate('Comments') . ": " . escape($_POST['comments']) . "</h3>
            <hr> " . $productsList . "
        </body>
        </html>
        ";

        $headers[] = 'MIME-Version: 1.0';
        $headers[] = 'Content-type: text/html; charset=utf-8';
        $headers[] = 'To: ' . ADMIN_EMAIL;
        $headers[] = 'From: ' . SERVER_EMAIL;

        // Send the mail to admin's email address and empty the cart
        if (!mail(ADMIN_EMAIL, $subject, $message, implode("\r\n", $headers))) {
            // If the mail could not be sent, set error
            $globalErrors[] = translate('Something went wrong, mail could not be sent.');
        } else {
            // If everything is all right
            // Insert order details in 'orders' table
            $sql = 'INSERT INTO orders (name, contact, comments, created_at) VALUES (?, ?, ?, ?);';
            $stmt = $pdo->prepare($sql);
            $stmt->execute([$_POST['name'], $_POST['contact'], $_POST['comments'], date('Y-m-d')]);

            // Get the order id after above insertion
            $sql = 'SELECT LAST_INSERT_ID();';
            $stmt = $pdo->prepare($sql);

            $stmt->execute();
            $orderId = $stmt->fetch();

            // Insert in 'product_order' table
            $sql = 'INSERT INTO product_order (product_id, order_id) VALUES(?, ?);';
            $stmt = $pdo->prepare($sql);
            foreach ($products as $product) {
                $stmt->execute([$product['id'], $orderId[0]]);
            }

            // Empty cart and flag success
            $_SESSION['cartList'] = $products = array();
            $checkoutSuccess = true;
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?= escape(translate('Cart')) ?></title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <?php if (!empty($products)) : ?>
        <div id="cartList">
            <?php foreach ($products as $product) : ?>
                <div class="product">
                    <img class="productImage" src="/images/<?= escape($product['image_name']) ?>" alt="<?= escape($product['title']) ?>">
                    <div class="productInfo">
                        <div class="productTitle"><?= escape($product['title']) ?></div>
                        <div class="productDescription"><?= escape($product['description']) ?></div>
                        <div class="productPrice"><?= escape($product['price']) ?></div>
                    </div>
                    <a href="cart.php?id=<?= escape($product['id']) ?>"><?= escape(translate("Remove")) ?></a>
                </div>
            <?php endforeach ?>
        </div>
    <?php endif ?>

    <?php if($checkoutSuccess) : ?>
        <p class="success"><?= escape(translate('Mail successfully sent!')) ?></p>
    <?php elseif (!empty($globalErrors)) : ?>
        <p class="error">
            <?php foreach ($globalErrors as $error) : ?>
                <?= escape($error) ?> <br>
            <?php endforeach ?>
        </p>
    <?php endif ?>

    <form method="POST">
        <input type="text"
               name="name"
               value="<?= escape($name) ?>"
               placeholder="<?= escape(translate('Name')) ?>">

        <?php if (!empty($nameErrors)) : ?>
            <p class="error">
                <?php foreach ($nameErrors as $error) : ?>
                    <?= escape($error) ?> <br>
                <?php endforeach ?>
            </p>
        <?php else : ?>
            <br><br>
        <?php endif ?>

        <input type="text"
               name="contact"
               value="<?= escape($contact) ?>"
               placeholder="<?= escape(translate('Contact details')) ?>">

        <?php if (!empty($contactErrors)) : ?>
            <p class="error">
                <?php foreach ($contactErrors as $error) : ?>
                    <?= escape($error) ?> <br>
                <?php endforeach ?>
            </p>
        <?php else : ?>
            <br><br>
        <?php endif ?>

        <textarea name="comments" placeholder="<?= escape(translate('Comments')) ?>"><?= escape($comments) ?></textarea>
        <br><br>

        <a href="/index.php"><?= escape(translate('Go to index')) ?></a>
        <button type="submit"><?= escape(translate('Checkout')) ?></button>
    </form>
</body>
</html>
