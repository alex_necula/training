<?php

require_once 'common.php';

// Value of username input
$username = '';
$loginErrors = $usernameErrors = $passErrors = array();

// If the login button is pressed, checks if the username and password match the values from config
if (isset($_POST['username'])) {
    // Both username and password fields MUST NOT be empty
    if (strlen($_POST['username']) === 0) {
        $usernameErrors[] = translate('Username field is empty.');
    }
    if (strlen($_POST['passw']) === 0) {
        $passErrors[] = translate('Password field is empty.');
    }

    // If fields are not empty, check for validation
    if (empty($usernameErrors) && empty($passErrors)) {
        if ($_POST['username'] == ADMIN_UNAME && $_POST['passw'] == ADMIN_PASS) {
            $_SESSION['username'] = true;

            header('Location: /index.php');
            exit();
        } else {
            $loginErrors[] = translate('Username and password invalid.');
        }
    }
    $username = $_POST['username'];
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?= escape(translate("Login")) ?></title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <?php if (!empty($loginErrors)) : ?>
        <p class="error">
            <?php foreach ($loginErrors as $error) : ?>
                <?= escape($error) ?> <br>
            <?php endforeach ?>
        </p>
    <?php endif ?>

    <?php if (!isset($_SESSION['username'])) : ?>
        <form method="POST">
            <input type="text" name="username" placeholder="<?= escape(translate("Username")) ?>" value="<?= escape($username) ?>">

            <?php if (!empty($usernameErrors)) : ?>
                <p class="error">
                    <?php foreach ($usernameErrors as $error) : ?>
                        <?= escape($error) ?> <br>
                    <?php endforeach ?>
                </p>
            <?php else : ?>
                <br><br>
            <?php endif ?>

            <input type="password" name="passw" placeholder="<?= escape(translate("Password")) ?>">

            <?php if (!empty($passErrors)) : ?>
                <p class="error">
                    <?php foreach ($passErrors as $error) : ?>
                        <?= escape($error) ?> <br>
                    <?php endforeach ?>
                </p>
            <?php else : ?>
                <br><br>
            <?php endif ?>

            <button type="submit"><?= escape(translate("Login")) ?></button>
        </form>
    <?php endif ?>
</body>
</html>
